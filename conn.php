<?php
include_once('config.php');

class Model
{
    public $db = null;
    function __construct()
    {
        try {
            self::openDatabaseConnection();
        } catch (\PDOException $e) {
            exit('Database connection could not be established.');
        }
    }
    private function openDatabaseConnection()
    {
        $options = array( PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
        // PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        if (DB_TYPE == "pgsql") {
            $databaseEncodingenc = " options='--client_encoding=" . DB_CHARSET . "'";
        } else {
            $databaseEncodingenc = "; charset=" . DB_CHARSET;
        }
        $this->db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . $databaseEncodingenc, DB_USER, DB_PASS, $options);
    }
}


// $a = new Model();

class Book extends Model
{
    public function getAll()
    {
        $sql = " SELECT  * FROM women ";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function multiInset(array $url)
    {
        
        $data = $this->convertToString($url);
        $sql = "INSERT INTO women (name) VALUES  $data ";
      
        $query = $this->db->prepare($sql);
        
        $query->execute();
    }

    public function convertToString($data) : string
    {
        $count = count($data);
        $str = '';

        for ($i=0; $i < $count; $i++) { 
            $str .= " ('$data[$i]')," ;    
        }

        return substr($str, 0, -1);
    }

    public function deleteDuplicate()
    {
        $sql = "
        DELETE t1 FROM women t1
            INNER JOIN
                women t2 
            WHERE
                t1.id < t2.id AND t1.name = t2.name;";
        $query = $this->db->prepare($sql);
       // $parameters = array(':book_id' => filter_var($book_id, FILTER_VALIDATE_INT));
        $query->execute();
        //$query->execute($parameters);
    }
}

$a = new Book();
$arr = ['aaaa','vvvv','cccccc','fffffffff'];
//print_r($a->convertToString($arr));
print_r($a->multiInset($arr));
print_r($a->deleteDuplicate());